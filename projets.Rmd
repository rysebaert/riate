---
title: "Projets"
output: 
  html_document: 
    number_sections: no
    toc: no
    includes:
      in_header: favicon.html 
---

```{r, echo = FALSE}
#devtools::install_github("gadenbuie/metathis@main")
library(metathis)
meta() %>%
  meta_description(
    "RIATE - Contrats, collaborations, réseaux"
  )
```


![](fig/banner_project.png)

Le RIATE se situe au cœur d’un réseau de recherche appliquée, qui comprend des entités partenaires en Europe, en France ainsi que des acteurs inscrits dans le périmètre de ses établissements de tutelle. Dans le cadre de ce réseau et de son expertise, il participe à différents types de projets scientifiques.

<br>

# {.tabset .tabset-fade .tabset-pills}

## Contrats européens et nationaux


Depuis sa création, l'équipe a été impliquée dans de nombreux contrats européens et nationaux, en tant que chef de file ou partenaire. Si la plupart de ces projets (13 dont 8 en position de coordinateur) relèvent initialement du programme de recherche ESPON (*European Territorial Observatory Network*), les destinataires se sont diversifiés au fil des années : Parlement et Commission européenne (4), Agence Nationale de la Cohésion Territoriale (7), Agence Nationale de la Recherche (1). 

```{r, eval = TRUE, echo=FALSE, warning=FALSE}
library(DT)
library(readxl)

meta <- data.frame(read_excel("docs/projets.xls", sheet = "projets"))
colnames(meta) <- c("Financeur", "Intitulé", "Mots-clés", "Période", "Leader", "Résultats")

datatable(meta, rownames = FALSE, escape = FALSE, 
          class = "hoover",
          options = list(dom = 't',  pageLength = 50, 
                         columnDefs = list(list(className = 'dt-center', targets = c(0,2:4),
                                                className = "df-left", targets = c(1,5)))))
```


## Collaborations - projets

L'équipe est impliquée dans des projets de collaboration scientifique dans le domaine des sciences de l'information géographique et des sciences reproductibles (liste non exhaustive).

```{r, eval = TRUE, echo=FALSE}
meta <- data.frame(read_excel("docs/collab.xls", sheet = "collab"))

library(kableExtra)
kbl(meta, align = "clc") %>%
  kable_styling(bootstrap_options = c("striped", "hover"), font_size =13)
```


## Réseaux

A côté de son implication dans des projets et collaborations scientifiques, le RIATE et ses ingénieurs font partie de différents réseaux. Ces réseaux peuvent être institutionnels, métiers, associatifs... (liste non exhaustive). 

```{r, eval = TRUE, echo=FALSE}
meta <- data.frame(read_excel("docs/reseaux.xls", sheet = "reseaux"))
meta <- meta[,c(2:4)]
colnames(meta) <- c("Nom", "Implication", "Type de réseau")

kbl(meta, align = "ccl") %>%
  kable_styling(bootstrap_options = c("striped", "hover"), font_size =13)
```
